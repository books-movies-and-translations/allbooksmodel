# AllBooksModel

This project contains only the model files for the AllBooks-Project.
These are written in the yaml notation following the OpenApi-Specification.
The server-project as well as the GUI-project are using these models to keep the model consistent.
The models are being used to generate the domain classes in both of these projects. 
Code-Generation takes place via https://openapi-generator.tech/.

## Integrate with your tools

I recommend using IntelliJ (Ultimate) for all the projects because the integration for editing yaml files using the 
OpenApi-Spec is quite good and developing in Kotlin and Angular/Typescript works also quite well.

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
